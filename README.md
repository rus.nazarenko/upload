### Application launch 
docker-compose up --build


### Addresses for requests
POST localhost:3001/api/user/avatar
{
  userId: 1,
  avatar: obivan.jpg (file)
}


GET localhost:3001/api/user/avatar
{
  "userId": "1"
}

For browsers: http://localhost:3001/amidala.jpg