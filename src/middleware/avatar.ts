import express from 'express'
import multer from 'multer'
import path from 'path'

  const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.join(__dirname, '../../static'))
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname)
    }
  })
  
  const upload = multer({ storage: storage })

  export const setAvatarMW = upload.single('avatar')