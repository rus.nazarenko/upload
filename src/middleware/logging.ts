import path from 'path'
import morgan from 'morgan'
import * as rfs from 'rotating-file-stream'


export const logging = () => {

  const accessLogStream = rfs.createStream('access.log', {
    interval: '1d',
    path: path.join(__dirname, '../log')
  })

  morgan.token('date', () => {
    return new Date().toLocaleString('en-US', { hour12: false })
  })

  return morgan('combined', { stream: accessLogStream })
}