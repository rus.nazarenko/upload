import { registerUserService, loginUserService, setAvatarService, getAvatarService } from './userService'
import { User } from './userTypes'
import { Request, Response } from 'express'



export const registerUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }
    const newUser = await registerUserService(data)

    res.status(201).json({ success: true, newUser })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const loginUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      password: req.body.password,
      email: req.body.email
    }
    const token = await loginUserService(data)
    res.cookie('token', `Bearer ${token}`)
    res.status(201).json({ success: true, token: token })

  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}


export const setAvatarController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }
    if (Object.entries(req.file).length === 0) { throw new Error("No file") }

    await setAvatarService(req.body.userId, req.file.path)
    res.status(201).json({ success: true })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}

export const getAvatarController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }
    const avatar = await getAvatarService(req.body.userId)
    res.status(201).json({ success: true, pathToAvatar: avatar.dataValues.avatar})
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}
