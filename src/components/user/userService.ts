const db = require("../../db/models")
import { User } from "./userTypes"
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'


export const registerUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (currenUser) throw new Error("This email is already registered")
    data.password = await bcrypt.hash(data.password, 10)
    const result = await db.user.create(data)
    return result
  } catch (err) {
    throw new Error(err)
  }
}


export const loginUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (!currenUser) throw new Error("Wrong email")
    const passwordTrue = await bcrypt.compare(data.password, currenUser.password)
    if (!passwordTrue) throw new Error("Wrong password")
    const token = jwt.sign(currenUser.dataValues.email, secret)
    return token
  } catch (err) {
    throw new Error(err)
  }
}


export const setAvatarService = async (userId: Number, path: String) => {
  try {
    const result = await db.user.update({ avatar: path }, { where: { id: userId } })
    if (result[0]) return
    throw new Error("No user")
  } catch (err) {
    throw new Error(err)
  }
}

export const getAvatarService = async (userId: Number) => {
  try {
    const user = await db.user.findOne({ where: { id: userId } })
    console.log("user ===>>>", user)
    if(!user)throw new Error("No user")
    return user
  } catch (err) {
    throw new Error(err)
  }
}
