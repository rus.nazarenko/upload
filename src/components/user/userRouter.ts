import express from 'express'
const userRouter = express.Router()
import { registerUserController, loginUserController, setAvatarController, getAvatarController } from './userController'
import { setAvatarMW } from '../../middleware/avatar'

userRouter.post('/register', registerUserController)
userRouter.post('/login', loginUserController)
userRouter.post('/avatar', setAvatarMW, setAvatarController)
userRouter.get('/avatar', getAvatarController)


export default userRouter