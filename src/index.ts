import express, { json, Application } from 'express'
import cookieParser from 'cookie-parser'
import { port } from './config/server'
import userRouter from "./components/user/userRouter"
import someRouter from "./components/some/someRouter"
import { tokenVerification } from './middleware/tokenVerification'
import { logging } from './middleware/logging'

const app: Application = express()


app.use(logging())
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))
app.use(json())
app.use(cookieParser())
app.use(express.static('static'))


// Routes without authentication
app.use('/api/user', userRouter)

// Authenticated routes
// app.use('/api/auth', tokenVerification)
app.use('/api/some', tokenVerification, someRouter)

app.listen(port, () => {
  console.log(`The server is running on http://localhost:${port}`)
})