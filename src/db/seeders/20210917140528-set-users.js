'use strict';
const {Op} = require('sequelize');
const { Sequelize } = require('../models');


const users = [
  {
    name: "Ruslan",
    email: "Ruslan@mail.com",
    password: 123456,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "Vera",
    email: "Vera@mail.com",
    password: 123456,
    avatar: "/server/static/amidala.jpg",
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
  {
    name: "James",
    email: "James@mail.com",
    password: 123456,
    created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
    updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
  },
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("users", users)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", {
      email: {
        [Op.in]: users.map((it) => it.email),
      },
    });
  }
}